const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.createOrder = async (reqBody) => {
  console.log(reqBody);

  let totalAmount = 0;
  const productsData = [];

  for (const product of reqBody.products) {
    const foundProduct = await Product.findById(product.productId);
    if (foundProduct) {
      totalAmount += product.quantity * foundProduct.price;
      productsData.push({
        productId: foundProduct._id,
        quantity: product.quantity,
      });
    }
  }

  const orderData = {
    userId: reqBody.userId,
    products: productsData,
    totalAmount: totalAmount, // Set the totalAmount field
  };

  const newOrder = new Order(orderData);

  try {
    const savedOrder = await newOrder.save();
    return true;
  } catch (error) {
    console.error(error);
    return false;
  }
};



module.exports.getAllOrders =() =>{
    return Order.find({}).then(result =>{
        return result;
    })
}
