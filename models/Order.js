const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "UserId id required"]
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "ProductId id required"]
			},
			quantity: {
				type: Number,
				required: [true, "quantity is reqired"]
			}
		}
	],
	totalAmount: {
		type: Number,
		required: true
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("Order", orderSchema);