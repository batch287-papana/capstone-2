const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");

router.post("/", (req,res) => {
	orderController.createOrder(req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/all",auth.verify,(req,res)=>{

    orderController.getAllOrders().then(resultFromController => res.send(
        resultFromController));
});


module.exports = router;